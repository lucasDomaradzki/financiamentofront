import React from 'react';
import InputMask from 'react-input-mask';
import Calendar from '../components/Calendar';

class Form extends React.Component {
  state = { 
    nome: '',
    cpf: '',
    dataNascimento: '',
    telefone: '',
    email: '',
    salario: '',
    fgts: '',
    valorImovel: '',
    prazoFinanciamento: '',
    taxaEntrada: '',
    taxaJurosAnual: '',
    enviarEmail: false,
    aceito: false // FIXME CONTINUAR DAQUI
  };

  handleChange = event => {
    let valor = event.target.value;

    valor = valor.replace(/\.| |-|\//gi, '');
    if ( valor.includes(",") ) {
      valor = valor.split(/,/gi, 1);
      this.setState({[event.target.name]: valor});
    } else  {
      this.setState({[event.target.name]: valor});
    }
  }

  handleChangeEmail = event => {
    this.setState({[event.target.name]: event.target.value});
  }

  toggleChange = event => {
    this.setState({[event.target.name]: event.target.checked});
  }

  onFormSubmit = event => {
    event.preventDefault();
    this.props.onFormSubmit( this.state );
  }

  cleanFiedls = () => {
    document.getElementById("formulario").reset();
  }

  render() {
    return (
      <form onSubmit={this.onFormSubmit} className="ui form" id="formulario">
        <h4 className="ui dividing header">DADOS PESSOAIS</h4>
        <div className="field">
          <div className="three fields">
            <div className="field">
              <label>Nome completo</label>
              <input type="text" name="nome" value={this.state.nome} onChange={this.handleChange} placeholder="Nome"/>
            </div>
            <div className="field">
              <label>CPF</label>
              <InputMask mask="999.999.999-99" type="text" name="cpf" onChange={this.handleChange} placeholder="000.000.000-00"/>
            </div>
            <div className="field">
              <label>Data de nascimento</label>
              <InputMask mask="99/99/9999" type="text" name="dataNascimento" onChange={this.handleChange} placeholder="dd/mm/aaaa"/>
            </div>
          </div>
          <div className="field">
          <div className="two fields">
            <div className="field">
              <label>Telefone</label>
              <InputMask mask="99 9 9999-9999" type="text" name="telefone" value={this.state.telefone} onChange={this.handleChange} placeholder="90 91989-8877"/>
              {/* <InputMask mask="+4\9 99 999 99" maskChar=" " type="text" name="telefone" value={this.state.telefone} onChange={this.handleChange} placeholder="90 91989-8877"/> */}
              {/* <input type="text" name="telefone" onChange={this.handleChange} placeholder="90 919898877"/> */}
            </div>
            <div className="field">
              <label>E-mail</label>
              <input type="text" name="email" onChange={this.handleChangeEmail} placeholder="email@email.com"/>
            </div>
          </div>
        </div>
        </div>
        <div className="field">
          <h4 className="ui dividing header">DADOS FINANCEIROS</h4>
          <div className="two fields">
            <div className="field">
              <label>Salário</label>
              <InputMask mask="9.999,00" type="text" name="salario" onChange={this.handleChange} placeholder="R$ 0.000,00"/>
              {/* <input type="text" name="salario" onChange={this.handleChange} placeholder="0.000,00"/> */}
            </div>
            <div className="field">
              <label>FGTS</label>
              <InputMask mask="99.999,00" type="text" name="fgts" onChange={this.handleChange} placeholder="R$ 0.000,00"/>
              {/* <input type="text" name="fgts" onChange={this.handleChange} placeholder="0.000,00"/> */}
            </div>
          </div>
        </div>
        <div className="field">
          <h4 className="ui dividing header">DADOS DO FINANCIAMENTO</h4>
          <div className="four fields">
            <div className="field">
              <label>Valor do imóvel</label>
              <InputMask mask="999.999,00" type="text" name="valorImovel" onChange={this.handleChange} placeholder="R$ 0.000,00"/>
              {/* <input type="text" name="valorImovel" onChange={this.handleChange} placeholder="0.000,00"/> */}
            </div>
            <div className="field">
              <label>Prazo de financiamento</label>
              <InputMask mask="999" type="text" name="prazoFinanciamento" onChange={this.handleChange} placeholder="360 meses"/>
              {/* <input type="text" name="prazoFinanciamento" onChange={this.handleChange} placeholder="360 meses"/> */}
            </div>
            <div className="field">
              <label>Taxa entrada</label>
              <InputMask mask="99.999,00" type="text" name="taxaEntrada" onChange={this.handleChange} placeholder="R$ 0.000,00"/>
              {/* <input type="text" name="taxaEntrada" onChange={this.handleChange} placeholder="0.000,00"/> */}
            </div>
            <div className="field">
              <label>Taxa de juros anual</label>
              <InputMask mask="99" type="text" name="taxaJurosAnual" onChange={this.handleChange} placeholder="15%"/>
              {/* <input type="text" name="taxaJurosAnual" onChange={this.handleChange} placeholder="2 %"/> */}
            </div>
          </div>
          <div className="field">
            <div className="ui toogle checkbox">
              <input type="checkbox" name="enviarEmail" value={this.state.enviarEmail} onChange={this.toggleChange}/>
              <label>Incluir envio de e-mail?</label>
            </div>
          </div>
        </div>
        <div className="field">
          <div className="three fields">
            <div className="field">
              <input type="submit" className="ui button" value="Simular Financiamento"/>
            </div>
            <div className="field">
              <input type="button" className="ui button" value="Limpar Campos" onClick={this.cleanFiedls}/>
            </div>
            <div className="field">
              <input type="button" className="ui button" value="Aceitar condições" onClick={this.aceitarCondicoes}/>
            </div>
          </div>
        </div>
      </form>
    );
  }
}

export default Form;