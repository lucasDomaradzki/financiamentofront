import React from 'react';
import Form from './Form';
import SimulacaoFinanciamento from './SimulacaoFinanciamento';
import Environment from '../environment/Environment';
import "../css/App.css";
import axios from 'axios';

class App extends React.Component {

  state = { 
    resultados: []
  }

  

  onFormSubmit = async props => {
    const response = await axios.post( Environment.apiBaseUrl+"/simularFinanciamento", {
      props
    }, {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Methods': 'GET, POST'
      },
    });

    this.setState({ resultados: response.data });
  };

  render()  {
    return (
      <div className="ui container containerPrincipal">
        <div className="ui grid">
          <div className="ui row">
            <div className="nine wide column">
              <Form onFormSubmit={this.onFormSubmit}/>
            </div>
            <div className="seven wide column">
                <h4 className="ui dividing header">RESULTADO</h4>
                <SimulacaoFinanciamento resultados={this.state.resultados}/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;