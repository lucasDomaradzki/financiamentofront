import React from 'react';
import ItemSimulacao from './ItemSimulacao';

const SimulacaoFinanciamento = ( { resultados } ) => {
  const listaSimulacao = resultados.map( ( resultado ) => {
    return <ItemSimulacao item={resultado}/>
  })

  return <div className="ui relaxed divided list">{listaSimulacao}</div>
}

export default SimulacaoFinanciamento;