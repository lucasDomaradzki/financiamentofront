import React from 'react';
import ReactCalendar from 'react-calendar';

class Calendar extends React.Component {

  state = { dataNascimento: "" };

  render()  {
    return (
      <div className="ui modal">
        <i className="close icon"></i>
        <div className="header">Selecione sua data de Nascimento</div>
        <div className="actions">
          <ReactCalendar/>
        </div>
      </div>
    );
  }
}

export default Calendar;